# -*- coding: utf-8 -*-
'''
@author: ljh
0528 company
回文进制数
'''

num = 11

while True:
    if str(num)==str(num)[::-1] and bin(num)[2:]==bin(num)[2:][::-1] and oct(num)[1:]==oct(num)[1:][::-1]:
        print num
        break
    num += 2